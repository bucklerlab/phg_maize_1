import htsjdk.variant.vcf.*
String gvcfPath="/Users/edbuckler/temp/mlw22reads/GVCFs/PostFilter/W22/W22_haplotype_caller_output_filtered.g.vcf.gz"
System.out.println("Opening the file")
String asmPath="/Users/edbuckler/temp/mlw22reads/assembly/exportVCForTaxonMethod_MaggieW22_includeInterAnchor.vcf.gz"

intervalsToTest=[[58777,67405]]//,
//                 [113475,115223],
//                 [177003,187314],
//                 [321017,324165],
//                 [324393,337947],
//                 [469913,471219],
//                 [482859,485921],
//                 [589400,590771],
//                 [593344,594024],
//                 [639021,642788]]

import htsjdk.variant.variantcontext.VariantContext
hapcallReader=new VCFFileReader(new File(gvcfPath))
asmReader=new VCFFileReader(new File(asmPath))
printUnknown=false

getVCGenotype= {VariantContext vc, int currentPosition ->
    if(currentPosition>= vc.getStart() && currentPosition<=vc.getEnd()) {
        geno=(vc.getGenotype(0).isHomRef())?0:1;
    } else {
        geno=2
    }
    return geno
}

def callForEach=new int[3][3]  //[ASM call][HCV call] - 0=Ref, 1=Alt, 2=NA
errorPos = []
correctAltPos=[]

intervalsToTest.forEach{interval ->
    asmIterator=asmReader.query("9",interval[0],interval[1]).iterator()
    asmVariant=asmIterator.next()
    println "ASM:"+asmVariant
    hcvIterator=hapcallReader.query("9",interval[0],interval[1]).iterator()
    hcvVariant=hcvIterator.next()
    println "HCV:"+asmVariant

    for (int pos = interval[0]; pos < interval[1]; pos++) {
//    println pos
        if(!hcvIterator.hasNext() || !asmIterator.hasNext() || asmVariant==null || hcvVariant==null) break;
        while(asmVariant.getEnd()<pos) asmVariant=asmIterator.next()
        while(hcvVariant.getEnd()<pos) hcvVariant=hcvIterator.next()

        asmCall=getVCGenotype(asmVariant,pos)
        hcvCall=getVCGenotype(hcvVariant,pos)
        if(pos==58783) {
            println "Error at "+pos+"\n"+"ASM:"+asmVariant +"\n" + "HCV:"+hcvVariant

            println pos +"\t"+asmCall+"\t"+hcvCall
        }
        callForEach[asmCall][hcvCall]++
        if(asmCall==2 || hcvCall==2) {if (printUnknown) println "ASM or HC Unknown at "+pos+"\n"+"ASM:"+asmVariant +"\n" + "HCV:"+hcvVariant}
        else if(asmCall!=hcvCall) println "Error at "+pos+"\n"+"ASM:"+asmVariant +"\n" + "HCV:"+hcvVariant
        if(asmCall==1 && hcvCall==1) correctAltPos << pos
        if(asmCall!=hcvCall && asmCall+hcvCall<2) errorPos << pos
    }

}
println correctAltPos
println errorPos
println callForEach