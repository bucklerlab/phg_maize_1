#!/bin/bash

license=cbsulogin2.tc.cornell.edu:8990
export SENTIEON_LICENSE=$license
RELEASE=/programs/sentieon-genomics-201711.01

$RELEASE/bin/sentieon driver \
-r /workdir/jav246/PHGtrainingMaterial_June/reference_genomes/maize/Zea_mays.AGPv4.dna.toplevelMtPtv3.fa  \
-t 20 \
-i /workdir/jav246/PHGtrainingMaterial_June/BAMs/W22/W22_HGCJCBGXX_GCCAAT_srt_dedup.bam \
--algo Haplotyper \
--emit_mode gvcf \
--ploidy 2 \
/workdir/jav246/PHGtrainingMaterial_June/GVCFs/W22_HGCJCBGXX_GCCAAT_srt_dedup_ploidy2.g.vcf

