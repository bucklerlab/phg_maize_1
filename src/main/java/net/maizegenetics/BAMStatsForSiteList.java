package net.maizegenetics;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.variant.variantcontext.VariantContext;
import htsjdk.variant.vcf.VCFFileReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class BAMStatsForSiteList {
    //http://lindenb.github.io/jvarkit/Biostar214299.html

    private static String rootDir = "/Users/edbuckler/temp/mlw22reads";
    //private static String bamInfile=rootDir+"Zm_allChr_5x_fullRef.sort.bam";
    //private static String bamInfile = rootDir + "Zm_allChr_5x_fullRef_final.sort.bam";
    //private static String bamInfile = rootDir + "Zm_allChr_5x_oddRef_final.sort.bam";
    private static List<String> bamInfiles = Arrays.asList(rootDir + "/W22/W22_HGCJCBGXX_GCCAAT_srt_dedup.bam",
            rootDir + "/W22/W22_HVMFTCCXX_L7.clean_srt_dedup.bam",
            rootDir + "/W22/W22_HVMFTCCXX_L8.clean_srt_dedup.bam");

    private static String vcfInfile = rootDir + "/GVCFs/PreFilter/W22/gvcfs/W22_haplotype_caller_output.g.vcf";
    //private static String csvOutfile = rootDir + "Zm_allChr_5x_fullRef.sort.bam.csv";
    //private static String csvOutfile = rootDir + "Zm_allChr_5x_oddRef_final.60k.sort.bam.csv";
    private static String csvOutfile = rootDir + "/W22_ALL_3.180616a.vcf_bam.txt";


    private static class MinMaxMissing {
        final double min;
        final double max;
        final double missing;

        public MinMaxMissing(double min, double max, double missing) {
            this.min = min;
            this.max = max;
            this.missing = missing;
        }
    }

    //Transformation and scaling approaches for each attribute
    private static final ImmutableMap<String, MinMaxMissing> minMaxMap = ImmutableMap.<String, MinMaxMissing>builder()
            .put("NM", new MinMaxMissing(0d, 150d, -1d))
            .put("AS", new MinMaxMissing(0d, 150d, -1))
            .put("XS", new MinMaxMissing(0d, 150d, -1))
            .put("MQ", new MinMaxMissing(0d, 60d, -1))
            .put("DIST", new MinMaxMissing(-2000d, 2000d, -1))
            .put("INSSIZE", new MinMaxMissing(-2000d, 2000d, -1))
            .build();


    public static void main(String[] args) {

        System.out.println("Input files:" + bamInfiles.toString());
        List<SamReader> bamReaders = bamInfiles.stream()
                .map(bamInfile -> SamReaderFactory.makeDefault().open(new File(bamInfile)))
                .collect(Collectors.toList());
        VCFFileReader vcfReader = new VCFFileReader(new File(vcfInfile));
        Map<String, BiFunction<VariantContext, String, String>> vcfIDtoProcessFunc = new LinkedHashMap<>();
        String defaultValue = "-1";
        BiFunction<VariantContext, String, String> variantAttributeFunc = (vc, id) -> vc.getAttribute(id, defaultValue).toString();
        BiFunction<VariantContext, String, String> genotypeAttributeFunc = (vc, id) -> vc.getGenotype(0).getAnyAttribute(id).toString();
        BiFunction<VariantContext, String, String> plAttributeFunc = (vc, id) -> {
            if (vc.getGenotype(0).hasPL() == false) return defaultValue;
            int index = Integer.parseInt(id.substring(id.length() - 1));
            int[] pls = vc.getGenotype(0).getPL();
            return (index < pls.length) ? "" + pls[index] : defaultValue;
        };
        Predicate<VariantContext> hasAlleleDepth = vc -> vc.getGenotype(0).getAD() != null;


        vcfIDtoProcessFunc.put("Chr", (vc, id) -> vc.getContig());
        vcfIDtoProcessFunc.put("Start", (vc, id) -> vc.getStart() + "");
        vcfIDtoProcessFunc.put("RefAllele", (vc, id) -> vc.getReference().getBaseString());
        vcfIDtoProcessFunc.put("AltAllele", (vc, id) -> vc.getAlternateAllele(0).toString());
        vcfIDtoProcessFunc.put("Length", (vc, id) -> vc.getEnd() - vc.getStart() + 1 + "");
        vcfIDtoProcessFunc.put("HomRef", (vc, id) -> vc.getGenotype(0).isHomRef() ? "1" : "0");
        vcfIDtoProcessFunc.put("Het", (vc, id) -> vc.getGenotype(0).isHet() ? "1" : "0");
        vcfIDtoProcessFunc.put("HomVar", (vc, id) -> vc.getGenotype(0).isHomVar() ? "1" : "0");
        vcfIDtoProcessFunc.put("AD0", (vc, id) -> hasAlleleDepth.test(vc) ? vc.getGenotype(0).getAD()[0] + "" : defaultValue);
        vcfIDtoProcessFunc.put("AD1", (vc, id) -> hasAlleleDepth.test(vc) && vc.getGenotype(0).getAD().length > 2 ? "" + vc.getGenotype(0).getAD()[1] : defaultValue);
        vcfIDtoProcessFunc.put("GQ", genotypeAttributeFunc);
        vcfIDtoProcessFunc.put("PL0", plAttributeFunc);
        vcfIDtoProcessFunc.put("PL1", plAttributeFunc);
        vcfIDtoProcessFunc.put("PL2", plAttributeFunc);
        vcfIDtoProcessFunc.put("BaseQRankSum", variantAttributeFunc);
        vcfIDtoProcessFunc.put("ClippingRankSum", variantAttributeFunc);
        vcfIDtoProcessFunc.put("MQRankSum", variantAttributeFunc);
        vcfIDtoProcessFunc.put("RAW_MQ", variantAttributeFunc);
        vcfIDtoProcessFunc.put("ReadPosRankSum", variantAttributeFunc);
        //System.out.println(vcfReader.getFileHeader().getInfoHeaderLines());


        Map<String, BiFunction<SAMRecord, String, Double>> bamIDtoProcessFunc = new LinkedHashMap<>();
        int readLength = 150;
        BiFunction<SAMRecord, String, Double> samAttributeFunc = (sr, id) -> scaledStat((Integer) sr.getAttribute(id), id, readLength);
        bamIDtoProcessFunc.put("NM", samAttributeFunc);
        bamIDtoProcessFunc.put("AS", samAttributeFunc);
//        bamIDtoProcessFunc.put("XS", samAttributeFunc);
        bamIDtoProcessFunc.put("MQ", (sr, id) -> scaledStat(sr.getMappingQuality(), id, 60));
//        bamIDtoProcessFunc.put("INSSIZE", (sr,id) -> scaledStat((Integer)sr.getInferredInsertSize(), id,1000));
//        bamIDtoProcessFunc.put("STRAND", (sr,id) -> sr.getReadNegativeStrandFlag() ? 1.0 : 0.0);


        try (BufferedWriter writer = Files.newWriter(new File(csvOutfile), Charset.defaultCharset())) {
            writer.write(vcfIDtoProcessFunc.keySet().stream().collect(Collectors.joining("\t")));
            writer.write("\tRefReads\t");
            writer.write(bamIDtoProcessFunc.keySet().stream().map(s -> "Ref_" + s).collect(Collectors.joining("\t")));
            writer.write("\tAltReads\t");
            writer.write(bamIDtoProcessFunc.keySet().stream().map(s -> "Alt_" + s).collect(Collectors.joining("\t")));
            writer.newLine();
            int counter = 1;
            int refCnt = 0, altCnt = 0, nothingCnt = 0;
            for (VariantContext variantContext : vcfReader) {
                if (!variantContext.getContig().equals("9")) continue;
                //if (variantContext.getGenotype(0).getAD() == null) continue;  //only get variant sites
                //if (!(variantContext.getReference().getBaseString().length() == 1 && variantContext.getAlternateAllele(0).length() == 1)) continue; //only SNPs/or indels
                Multimap<String, SAMRecord> matchTypeToSAMRecord = HashMultimap.create(3, 10);
                bamReaders.forEach(bamReader -> matchTypeToSAMRecord.putAll(sortReadsByMatchingVCFCall(bamReader, variantContext)));
                refCnt += matchTypeToSAMRecord.get("REF").size();
                altCnt += matchTypeToSAMRecord.get("ALT").size();
                nothingCnt += matchTypeToSAMRecord.get("NA").size();
                if (refCnt % 100 == 0) System.out.printf("%d\t%d\t%d\n", refCnt, altCnt, nothingCnt);
                writer.write(reportVCF(vcfIDtoProcessFunc, variantContext));
                writer.write(reportBAM(bamIDtoProcessFunc, matchTypeToSAMRecord, "REF"));
                writer.write(reportBAM(bamIDtoProcessFunc, matchTypeToSAMRecord, "ALT"));
                writer.newLine();
                counter++;
                //if(counter>1000) break;
            }
            System.out.printf("%d\t%d\t%d\n", refCnt, altCnt, nothingCnt);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            for (SamReader bamReader : bamReaders) bamReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Output file:" + csvOutfile);
        System.out.println("ConvertSimSAMtoFastq completed");
    }

    private static String reportBAM(Map<String, BiFunction<SAMRecord, String, Double>> bamIDtoProcessFunc, Multimap<String, SAMRecord> matchTypeToSAMRecord, String refOrAlt) {
        StringBuilder sb = new StringBuilder();
        sb.append(matchTypeToSAMRecord.get(refOrAlt).size()).append("\t");
        Multimap<String, Double> bamAttToValue = LinkedHashMultimap.create();
        matchTypeToSAMRecord.get(refOrAlt).stream().forEach(samRecord -> {
            bamIDtoProcessFunc.forEach((id, func) -> bamAttToValue.put(id, func.apply(samRecord, id)));
        });
        bamIDtoProcessFunc.keySet().forEach(id -> {
            Collection<Double> values = bamAttToValue.get(id);
            if (!values.isEmpty()) {
                double average = values.stream().mapToDouble(d -> d).average().orElse(-1);
                sb.append(average).append("\t");
            } else {
                sb.append(-1).append("\t");
            }
        });
        return sb.toString();
    }

    private static String reportVCF(Map<String, BiFunction<VariantContext, String, String>> vcfIDToDescription, VariantContext variantContext) {
        StringBuilder sb = new StringBuilder();
        vcfIDToDescription.forEach((id, func) -> sb.append(func.apply(variantContext, id)).append("\t"));
        return sb.toString();
    }

    /**
     * Sorts reads by variants context in reference (REF), alternate (ALT), and No match (NA)
     *
     * @param bamReader
     * @param variantContext
     * @return a map of these records.
     */
    private static Multimap<String, SAMRecord> sortReadsByMatchingVCFCall(SamReader bamReader, VariantContext variantContext) {
        Multimap<String, SAMRecord> matchTypeToSAMRecord = HashMultimap.create(3, 10);
        SAMRecordIterator samRI = bamReader.queryOverlapping(variantContext.getContig(), variantContext.getStart(), variantContext.getStart() + 1);
        String refAllele = variantContext.getReference().getBaseString();
        String altAllele = variantContext.getAlternateAllele(0).getBaseString();
        //Second alternate alleles are not being tested and these are some of the NA class
        while (samRI.hasNext()) {
            SAMRecord samRecord = samRI.next();
            //System.out.println("samRecord = " + samRecord);
            int readpos = samRecord.getReadPositionAtReferencePosition(variantContext.getStart());
            if (readpos < 1) continue;
            readpos--;
            String read = samRecord.getReadString();
            boolean isRef = read.startsWith(refAllele, readpos);
            boolean isAlt = read.startsWith(altAllele, readpos);
            //the second test is for nested indels - it assigns by the longest match
            if ((isRef && !isAlt) || (isRef && isAlt && (refAllele.length() > altAllele.length()))) {
                matchTypeToSAMRecord.put("REF", samRecord);
            } else if ((isAlt && !isRef) || (isRef && isAlt && (refAllele.length() < altAllele.length()))) {
                matchTypeToSAMRecord.put("ALT", samRecord);
            } else {
                matchTypeToSAMRecord.put("NA", samRecord);
//                System.out.println("Nothing found:"+ read+ "\t"+refAllele+"\t"+altAllele+"\t"+readpos+"\t"+read.substring(readpos,Math.min(readpos+4, read.length()-1)));
            }
        }
        samRI.close();
        return matchTypeToSAMRecord;
    }

    private static double scaledStat(double value, String statName, int readlength) {
        MinMaxMissing mmm = minMaxMap.get(statName);
        double max = (readlength == Integer.MAX_VALUE) ? mmm.max : readlength;
        try {
            //double value = func.apply(statName);
            double scaledValue = (value - mmm.min) / (max - mmm.min);
            if (scaledValue < 0) scaledValue = 0;
            if (scaledValue > 1) scaledValue = 1;
            return scaledValue;
        } catch (NullPointerException e) {
            return mmm.missing;
        }
    }


    private static String centeredVector(int length, int centerOfInput, List<Double> inList, String prefix, double missingValue) {
        StringBuilder sb = new StringBuilder();
        if (prefix != null) sb.append(prefix + ",");
        int prefixBuffer = (length / 2) - centerOfInput;
        for (int i = 0; i < length; i++) {
            if (i < prefixBuffer) {
                sb.append(missingValue + ",");
            } else if (i >= inList.size() + prefixBuffer) {
                sb.append(missingValue + ",");
            } else sb.append(inList.get(i - prefixBuffer) + ",");
        }
        return sb.toString();
    }
}
