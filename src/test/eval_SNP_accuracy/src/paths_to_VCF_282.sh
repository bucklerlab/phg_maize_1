#!/bin/bash -x

mkdir -p /workdir/jlg374/paths
PATHDIR=/data1/PHG/HackathonFiles/Dec_2018/MinusGDB/Paths/

## Get R1 path files for all 282 (R1 and R2 are the forward and reverse reads)
scp jlg374@cbsublfs1.tc.cornell.edu:$PATHDIR/*/*R1_path.txt /workdir/jlg374/paths/
scp jlg374@cbsublfs1.tc.cornell.edu:$PATHDIR/*/*1.clean_path.txt /workdir/jlg374/paths/
scp -r jlg374@cbsublfs1.tc.cornell.edu:~/FilesForJoe/Tassel_PHG/ /workdir/jlg374/

cd /workdir/jlg374/Tassel_PHG

nohup ./PathsToVCF.sh &> PathsToVCF.log &

