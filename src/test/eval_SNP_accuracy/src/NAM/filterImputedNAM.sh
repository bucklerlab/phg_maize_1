#!/bin/bash

# This script gathers NAM genotypes imputed by Guillaume, concatenates them and indexes the result.
# Then it filters the NAM genotypes and some NAM SNPs called by the PHG down to a common set of sites.

mkdir -p /workdir/jlg374/beagle
mkdir -p /workdir/jlg374/beagle/orig
mkdir -p /workdir/jlg374/beagle/filtered
FILES=""

scp jlg374@cbsublfs1.tc.cornell.edu:/data1/users/gr226/NAM/Beagle_imputed/imputed/AGPv4_NAM_chr*.imputed.vcf.gz /workdir/jlg374/beagle/orig/
scp jlg374@cbsublfs1.tc.cornell.edu:/data1/users/gr226/NAM/Beagle_imputed/imputed/AGPv4_NAM_chr*.imputed.vcf.gz.tbi /workdir/jlg374/beagle/orig/

# Concatenate imputed NAM genotypes and index the result
for CHR in $(seq 10)
do
  BEAGLE=/workdir/jlg374/beagle/orig/AGPv4_NAM_chr${CHR}.imputed.vcf.gz
  FILES=$( echo "$FILES $BEAGLE" )
done

ALLCHR=/workdir/jlg374/beagle/AGPv4_NAM_chrALL.imputed.vcf.gz
bcftools concat -Oz -o $ALLCHR --threads 22 $FILES
bcftools index -f --threads 22 $ALLCHR

# Filter Beagle genotypes and PHG genotypes to match.  The 'DR2 < 0.8' filter is to remove
# sites with poor imputation, as recommended by Guillaume.
BEAGLE=/workdir/jlg374/beagle/AGPv4_NAM_chrALL.imputed.vcf.gz
PHG=/workdir/jlg374/NAM_CML247_CML103_P39.vcf.gz 
OUTDIR=/workdir/jlg374/intersect/

mkdir -p $OUTDIR
bcftools isec  -Oz --threads 24 -p $OUTDIR -e 'DR2 < 0.8' -e - -n =2 $BEAGLE $PHG &
NEWBEAGLE=$( basename $BEAGLE | sed 's/.vcf.gz/.filtered.vcf.gz/' )
NEWPHG=$( basename $PHG | sed 's/.vcf.gz/.filtered.vcf.gz/' )

# Rename output files
mv $OUTDIR/0000.vcf.gz $OUTDIR/$NEWBEAGLE
mv $OUTDIR/0000.vcf.gz.tbi $OUTDIR/${NEWBEAGLE}.tbi
mv $OUTDIR/0001.vcf.gz $OUTDIR/$NEWPHG
mv $OUTDIR/0001.vcf.gz.tbi $OUTDIR/${NEWPHG}.tbi


  


