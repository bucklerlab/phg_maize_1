#!/bin/bash -x

time tassel-5-standalone/run_pipeline.pl -Xmx400g -debug -HaplotypeGraphBuilderPlugin -configFile config.txt -methods mummer4 -includeVariantContexts true -endPlugin -ImportHaplotypePathFilePlugin -inputFileDirectory /workdir/jlg374/Tassel_PHG/Paths/ -endPlugin -PathsToVCFPlugin -outputFile /workdir/jlg374/NAM_CML247_CML103_P39.vcf -endPlugin
