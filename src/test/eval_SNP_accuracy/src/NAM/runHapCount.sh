#!/bin/bash -x

READ_DIR=/workdir/jlg374/new_fastq/
KMER_FILE=HammingIndex_MinusG_MinHamming2.bin
cd Tassel_PHG/ 
mkdir HapCounts/
mkdir Paths/
mkdir logs/



echo "Processing Hap Counts"

time tassel-5-standalone/run_pipeline.pl -Xmx100g -debug -HaplotypeGraphBuilderPlugin -configFile config.txt -methods mummer4 -endPlugin -FastqKmerToHapCountPlugin -configFile config.txt -kmerFile ${KMER_FILE} -kmerPrefix C -method HAP_COUNT_WGS_1 -readDirectory ${READ_DIR}/ -exportHaploFile HapCounts/countFile  -loadDb false -initialMapSize 500000000 -endPlugin > logs/hapCountWGSFixedTotalCount.log 2>&1

echo "Processing Find Path"


time tassel-5-standalone/run_pipeline.pl -Xmx100g -debug -HaplotypeGraphBuilderPlugin -configFile config.txt -methods mummer4 -endPlugin -HapCountBestPathToTextPlugin -configFile config.txt -inclusionFileDir HapCounts/ -outputDir Paths/ -hapCountMethod HAP_COUNT_WGS_1 -pMethod PATH_WGS -endPlugin > logs/paths.log 2>&1


