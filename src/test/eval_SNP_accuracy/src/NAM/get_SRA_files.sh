#!/bin/bash

DUMPDIR=~/PHG_SNPs/NAM/orig_fastq

sraList=(SRR391160 SRR391155 SRR391120)
# These have 45-80 lines from each of the CML247, CML103, and P39 NAM biparentals

for SRA in "${sraList[@]}"
do
  echo $SRA
  fastq-dump -O $DUMPDIR --gzip $SRA
done
