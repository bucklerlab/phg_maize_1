#!/bin/bash

KEYFILE=/workdir/jlg374/NAM_CML103_CML247_P39_keyfile.txt

# Rename files from SRA with as flowcell_lane_fastq.txt.gz
mkdir /workdir/jlg374/new_fastq
cp ~/PHG_SNPs/NAM/orig_fastq/SRR391160.fastq.gz /workdir/jlg374/new_fastq/70980AAXX_3_fastq.txt.gz
cp ~/PHG_SNPs/NAM/orig_fastq/SRR391155.fastq.gz /workdir/jlg374/new_fastq/705VVAAXX_5_fastq.txt.gz
cp ~/PHG_SNPs/NAM/orig_fastq/SRR391120.fastq.gz /workdir/jlg374/new_fastq/628NHAAXX_4_fastq.txt.gz

# This splits a single fastq up into single-sample fastqs by barcode
for FASTQ in $(ls /workdir/jlg374/new_fastq/*fastq.txt.gz)
do
  Tassel_PHG/tassel-5-standalone/run_pipeline.pl -Xmx400g -ConvertOldFastqToModernFormatPlugin -i $FASTQ -k $KEYFILE -p AmesGBSForPHG -o /workdir/jlg374/new_fastq/ -e ApeKI > logs/convert_${FASTQ##*/}.log 2>&1
done

rm /workdir/jlg374/new_fastq/*gz
