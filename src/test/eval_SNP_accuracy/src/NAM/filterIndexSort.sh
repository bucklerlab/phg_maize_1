#!/bin/bash


SORTEDFILE=$( echo $2 | sed 's/.vcf.gz/.sorted.vcf.gz/' )
bcftools filter -Oz -o $2 -e 'INFO/DR2 < 0.8' --threads 2 $1
bcftools index -f --threads 22 $2
bcftools sort -m 10G -Oz -o $SORTEDFILE $2

#rm $2

