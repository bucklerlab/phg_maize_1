#!/bin/bash -x

mkdir logs/

cp -r ~/FilesForJoe/Tassel_PHG /workdir/jlg374/

echo "Converting FASTQ files"
./convert_fastq_to_new_format.sh > logs/convertFastq.log

./runHapCount.sh

echo "Calling SNPs"
./PathsToVCF.sh > logs/pathsToVCF.log
