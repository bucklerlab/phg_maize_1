library(magrittr)
library(caret)
library(ggplot2)

source("~/projects/phg_maize_1/src/test/eval_SNP_accuracy/src/utility_functions.R")

minreads=0
path_to_PHG_SNPs <- paste0("/media/jlg374/Data/test_PHG_SNPs/Ames_subset_GBS_PHG_minReads", minreads, "_filterAxiom_sorted.vcf")
path_to_Axiom_SNPs <- "/media/jlg374/Data/test_PHG_SNPs/TUMPLANTBREEDING_Maize600k_elitelines_AGPv4_sorted.vcf"

############################################################################
## Source rtassel machinery - here through line 40 taken from initialTests.R
## Load packages
library(rJava) 
library(GenomicRanges)
library(stringr)
library(SummarizedExperiment)
library(snpStats)
library(hexbin)
library(testthat)

## Set WD
setwd("~/projects/rtassel")
path_tassel <- paste0(getwd(),"/inst/java/sTASSEL.jar")

## jinit
rJava::.jinit(parameters="-Xmx20g")
.jcall(.jnew("java/lang/Runtime"), "J", "totalMemory")
.jcall(.jnew("java/lang/Runtime"), "J", "maxMemory")

## Add class path
# Note the file class paths may differ between Windows and Macs.
homeloc <- Sys.getenv("HOME")

rJava::.jaddClassPath(path_tassel)
print(.jclassPath())

tasselVersion <- rJava::.jfield("net/maizegenetics/tassel/TASSELMainFrame","S","version")
str_c("Using TASSEL version: ",tasselVersion)

# rJava::.jaddClassPath("/Users/edwardbuckler/Code/tassel-5-source/dist/sTASSEL.jar")

## Source files
source("R/AllGenerics.R")
source("R/AllClasses.R")
source("R/TasselPluginWrappers.R")
source("R/PullFunctions.R")
source("R/PushFunctions.R")

# Logging for debugging:
rJava::.jcall("net.maizegenetics/util/LoggingUtils", "V", "setupDebugLogging")

############################################################################

axiom <- readGenotypeTable(path_to_Axiom_SNPs)
phg <- readGenotypeTable(path_to_PHG_SNPs)

# Convert to RangedSummarizedExperiment and subset to SNPs on Chromosomes 1-10
axiomSE <- summarizeExperimentFromGenotypeTable(axiom) %>% 
           subset(subset= seqnames %in% 1:10)

phgSE <- summarizeExperimentFromGenotypeTable(phg) %>% 
           subset(subset= seqnames %in% 1:10)

# Subset on common markers
axiom_to_compare <- subsetByOverlaps(axiomSE, phgSE)
phg_to_compare <- subsetByOverlaps(phgSE, axiomSE)

## Manually change phg_to_compare sample names:
phgNames <- read.table("~/projects/phg_maize_1/src/test/eval_SNP_accuracy/data/Ames_sample_key_withAxiom.txt",
                       sep="\t", stringsAsFactors=FALSE, header=TRUE)
phgNames$Samples <- sub("_path.txt", "", phgNames$Samples)
axiomNames <- read.table("~/projects/phg_maize_1/src/test/eval_SNP_accuracy/data/axiom_sample_key_withAmes.txt",
                         sep="\t", stringsAsFactors=FALSE, header=TRUE)
phgKeep <- phgNames$Samples[phgNames$Common]
axiomKeep <- axiomNames$Samples[match(phgNames$Taxon[phgNames$Common],
                                      axiomNames$Taxon)]
phg_to_compare <- phg_to_compare[,phgKeep]
axiom_to_compare <- axiom_to_compare[,axiomKeep]

# Remove hets
axiom_to_compare <- remove_SE_hets(axiom_to_compare)
phg_to_compare <- remove_SE_hets(phg_to_compare)

# Remove markers that are indel in either assay.  Indels are
# identified by checking the to see if reference or alternate alleles
# have >1 character.  Might be a better way to do this.
axiom_ref <- elementMetadata(axiom_to_compare)$refAllele
axiom_alt <- elementMetadata(axiom_to_compare)$altAllele
phg_ref <- elementMetadata(phg_to_compare)$refAllele
phg_alt <- elementMetadata(phg_to_compare)$altAllele

indel <- nchar(axiom_ref) > 1 | nchar(phg_ref) > 1 |
         nchar(axiom_alt) > 1 | nchar(phg_alt) > 1

axiom_to_compare <- axiom_to_compare[!indel, ]
phg_to_compare <- phg_to_compare[!indel, ]

# Compare genotype calls at all sites
conf <- lapply(1:ncol(axiom_to_compare), function(x) 
  makeColumnConfusionMatrix(x, axiom_to_compare, phg_to_compare))
conf <- do.call(rbind, conf)

# Calculate proportion of non-NA Axiom SNPs that are non-NA in PHG
missingSNPs <- colSums(is.na(assay(phg_to_compare)) & !is.na(assay(axiom_to_compare))) / 
               colSums(!is.na(assay(axiom_to_compare)))
power <- data.frame(Value=1-missingSNPs,
                    Taxon=colnames(axiom_to_compare),
                    Metric="Power")

toPlot <- rbind(power, conf)
toPlot_file <- paste0("../phg_maize_1/src/test/eval_SNP_accuracy/data/power_errorRate_minreads", minreads, "_AmesGBS.txt")
write.table(toPlot, toPlot_file, row.names=FALSE, col.names=TRUE, sep="\t", quote=FALSE)

# Plot proportion of correct SNP calls
ggplot(toPlot, aes(reorder(Taxon, Value, min), Value, fill=reorder(Taxon, Value, min))) +
  geom_col(position="dodge2") +
  #scale_fill_manual(values=c("cornflowerblue", "orange")) +
  facet_wrap(~Metric, scales = "free_y", ncol=1) +
  theme_classic() +
  labs(x="Taxon") +
  geom_hline(data=data.frame(yint=c(0.05, 0.9), Metric=c("Error Rate", "Power")), 
             aes(yintercept=yint), linetype="dotted") +
  theme(axis.text.x = element_text(angle=45, hjust=1),
        legend.position = "none")
power_error_file <- paste0("../phg_maize_1/src/test/eval_SNP_accuracy/figures/AmesGBS/power_errorRate.png")
ggsave(power_error_file, width=10, height=6)

# Plot proportion of correct calls for 64-SNP windows
# winsize=64
# matching <- assay(axiom_to_compare) == assay(phg_to_compare)
# binnedErrors <- zoo::rollapply(!matching, width=winsize, by=winsize, FUN=sum, na.rm=TRUE)
# binnedErrors_toPlot <- reshape2::melt(binnedErrors, value.name="NumErrors")
# colnames(binnedErrors_toPlot)[2] <- "Taxon"
# ggplot(binnedErrors_toPlot, aes(NumErrors)) +
#   geom_histogram(binwidth=1, center=0) +
#   xlim(1.5, 64) +
#   facet_wrap(~Taxon, ncol=6, scales = "free_y") +
#   theme_classic()
# ggsave("../phg_maize_1/src/test/eval_SNP_accuracy/figures/error_rate_64SNP_bins.png", width=10, height=6)

##################################################################
# Plot accuracy and power by reference range

rr_df <- read.table("/media/jlg374/Data/test_PHG_SNPs/refRanges.txt",
                    header=TRUE, stringsAsFactors=FALSE)

rr_range <- makeGRangesFromDataFrame(rr_df)

snp_range <- axiom_to_compare@rowRanges
elementMetadata(snp_range)$refRangeNum <- findOverlaps(rr_range, snp_range)@from

getRangeMean <- function(i) {
  snpsInRange <- subsetByOverlaps(snp_range, rr_range[i,])
  if(length(snpsInRange) == 0) return(NA)
  return(mean(snpsInRange$snpMatch, na.rm=TRUE))
}

rr_error <- data.frame()
for(col in 1:ncol(axiom_to_compare)){
  snpMatch <- assay(axiom_to_compare[,col]) == assay(phg_to_compare[,col])
  elementMetadata(snp_range)$snpMatch <- snpMatch
  this_rr_error <- aggregate(!snp_range$snpMatch, by=list(ReferenceRange=snp_range$refRangeNum), 
                        FUN=sum, na.rm=TRUE)
  colnames(this_rr_error)[2] <- "MiscalledSNPs"
  total_rr_snps <- aggregate(snp_range$snpMatch, by=list(ReferenceRange=snp_range$refRangeNum),
                             FUN=length)
  this_rr_error <- merge(this_rr_error, total_rr_snps)
  colnames(this_rr_error)[3] <- "TotalSNPs"
  this_taxon <- colnames(axiom_to_compare)[col]
  this_rr_error$Taxon <- this_taxon
  if(this_taxon %in% rr_error$Taxon){
    this_rr_error$Rep <- max(rr_error$Rep[rr_error$Taxon == this_taxon]) + 1
  } else{
    this_rr_error$Rep <- 1
  }
  rr_error <- rbind(rr_error, this_rr_error)
}

rr_error$Chromosome <- as.integer(seqnames(rr_range)[rr_error$ReferenceRange])
rr_error$Position <- start(rr_range)[rr_error$ReferenceRange] + (width(rr_range)[rr_error$ReferenceRange] / 2)
rr_error$Rep <- as.factor(rr_error$Rep)
rr_error$Start <- start(rr_range)[rr_error$ReferenceRange]

## Write out rr_error for use in Haploplot
# rr_out <- as.data.frame(rep(rr_range, each=6))
# rr_out$Taxon <- rep(colnames(axiom_to_compare), length(rr_range))
# error_out <- cbind(rr_error, rr_range[rr_error$ReferenceRange])
# rr_out <- merge(rr_out, error_out, all=TRUE)
# rr_out <- rr_out[,c("seqnames", "start", "end", "Value", "Taxon")]
# rr_out <- data.frame(ref_range_id=rep(1:(nrow(rr_out)/6), each=6),
#                      rr_out)
# colnames(rr_out) <- c(colnames(rr_df)[1:4], "n_incorrect_snps", "taxon")
# write.table(rr_out, "../eval_PHG_SNP_accuracy/error_by_ref_range.txt", 
#             row.names=FALSE, col.names=TRUE, sep="\t", quote=FALSE)

# Get error rate per SNP to look for problematic axiom SNPs
# errors <- assay(axiom_to_compare) != assay(phg_to_compare) 
# perSNPerrors <- rowSums(errors)
# 
# errorPlot <- data.frame(rowRanges(axiom_to_compare),
#                      ErrorRate=perSNPerrors,
#                      problematic=as.integer(perSNPerrors > (ncol(phg_to_compare)/2)))
# errorPlot <- errorPlot[errorPlot$problematic & !is.na(errorPlot$problematic),]
# colnames(errorPlot)[1] <- "Chromosome"


for(i in unique(rr_error$Taxon)){
  ggplot(rr_error[rr_error$Taxon == i, ], aes(Position, MiscalledSNPs, color=Rep)) +
    geom_line() +
    facet_wrap(~Chromosome,ncol=1) +
    labs(y="Number of Incorrect SNP Calls", title=i) +
    theme_classic() +
    theme(legend.position = 'none') +
    ylim(0, max(rr_error$MiscalledSNPs))
    # geom_point(data=errorPlot, aes(start, problematic-1), 
    #            size=2, pch="|", inherit.aes = FALSE)
  outpath <- paste0("../phg_maize_1/src/test/eval_SNP_accuracy/figures/AmesGBS/error_rate_by_chrom_", i, ".pdf")
  ggsave(outpath, width=8, height=12)
}

#######################################################################
