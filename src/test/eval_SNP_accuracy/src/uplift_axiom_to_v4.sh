#!/bin/bash

# TUMPLANTBREEDING_Maize600k_elitelines_AGPv2.vcf.gz was originally 'TUMPLANTBREEDING_Maize600k_elitelines _AGPv2.vcf.gz'
# Files 'TUMPLANTBREEDING_Maize600k_elitelines _AGPv2.vcf.gz', 'AGPv2_to_AGPv4.chain.gz', 
#   and 'Zea_mays.AGPv4.dna.toplevelMtPtv3.fa' came from Zack Miller.

export PYTHONPATH=/programs/CrossMap-0.2.9/lib64/python2.7/site-packages:/programs/CrossMap-0.2.9/lib/python2.7/site-packages:$PYTHONPATH
export PATH=/programs/CrossMap-0.2.9/bin:$PATH

CrossMap.py vcf AGPv2_to_AGPv4.chain.gz TUMPLANTBREEDING_Maize600k_elitelines_AGPv2.vcf.gz Zea_mays.AGPv4.dna.toplevelMtPtv3.fa TUMPLANTBREEDING_Maize600k_elitelines_AGPv4.vcf
