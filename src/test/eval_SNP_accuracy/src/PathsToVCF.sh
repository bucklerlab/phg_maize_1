#!/bin/bash -x

time tassel-5-standalone/run_pipeline.pl -Xmx400g -debug -HaplotypeGraphBuilderPlugin -configFile config.txt -methods mummer4 -includeVariantContexts true -endPlugin -ImportHaplotypePathFilePlugin -inputFileDirectory /workdir/jlg374/paths -endPlugin -PathsToVCFPlugin -outputFile /workdir/jlg374/282_taxa_PHG_minReads1.vcf -endPlugin
