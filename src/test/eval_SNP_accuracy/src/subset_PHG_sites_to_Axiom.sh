ORIG_PATH=/workdir/jlg374/282_taxa_PHG_minReads1.vcf
OUT_PATH=/workdir/jlg374/282_taxa_PHG_minReads1_filterAxiom.vcf
SITE_FILE=/workdir/jlg374/Axiom/600k_sites.txt

vcftools --vcf $ORIG_PATH --positions $SITE_FILE --recode $OUT_PATH