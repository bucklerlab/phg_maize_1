#Notebook For HC Ref, ASM SNP Errors

###Running Chromosome 9 of W22 through the Mummer pipeline
* Normalized INDELs
* Resized surrounding Reference Blocks in GVCF for the normalized indels using ResizeRefBlockPlugin currently on PHG-201 branch

Using Ed's counting groovy script.  CompareHCWAssembly.groovy.  Reported Positions are in B73 agpv4 coordinates provided by the GVCF

###Discrepancies

####Error at 64588

ASM:[VC Unknown @ 9:64588 Q. of type=SNP alleles=[C*, T] attr={} GT=[[W22_Assembly T]]

HCV:[VC Unknown @ 9:64588 Q. of type=SYMBOLIC alleles=[C*, <NON_REF>] attr={END=64588} GT=[[W22 C* GQ 0 DP 1 PL 0,0 {MIN_DP=1}]]

####Error at 114107

ASM:[VC Unknown @ 9:114107 Q. of type=SNP alleles=[C*, T] attr={} GT=[[W22_Assembly T]]

HCV:[VC Unknown @ 9:114107 Q. of type=SYMBOLIC alleles=[C*, <NON_REF>] attr={END=114107} GT=[[W22 C* GQ 0 DP 1 PL 0,0 {MIN_DP=1}]]


####Error at 639728

ASM:[VC Unknown @ 9:639728 Q. of type=SNP alleles=[C*, G] attr={} GT=[[W22_Assembly G]]

HCV:[VC Unknown @ 9:639728 Q. of type=SYMBOLIC alleles=[C*, <NON_REF>] attr={END=639728} GT=[[W22 C* GQ 0 DP 2 PL 0,0 {MIN_DP=2}]]

####Error at 639743

ASM:[VC Unknown @ 9:639743 Q. of type=SNP alleles=[A*, T] attr={} GT=[[W22_Assembly T]]

HCV:[VC Unknown @ 9:639743 Q. of type=SYMBOLIC alleles=[A*, <NON_REF>] attr={END=639743} GT=[[W22 A* GQ 0 DP 1 PL 0,0 {MIN_DP=1}]]

####Error at 639753

ASM:[VC Unknown @ 9:639753 Q. of type=SNP alleles=[A*, G] attr={} GT=[[W22_Assembly G]]

HCV:[VC Unknown @ 9:639746-639900 Q. of type=SYMBOLIC alleles=[A*, <NON_REF>] attr={END=639900} GT=[[W22 A* GQ 0 DP 0 PL 0,0 {MIN_DP=0}]]

####Error at 640158

ASM:[VC Unknown @ 9:640158 Q. of type=SNP alleles=[A*, T] attr={} GT=[[W22_Assembly T]]

HCV:[VC Unknown @ 9:640158-640159 Q. of type=SYMBOLIC alleles=[A*, <NON_REF>] attr={END=640159} GT=[[W22 A* GQ 0 DP 9 PL 0,0 {MIN_DP=9}]]

####Error at 640159

ASM:[VC Unknown @ 9:640159 Q. of type=SNP alleles=[T*, G] attr={} GT=[[W22_Assembly G]]

HCV:[VC Unknown @ 9:640158-640159 Q. of type=SYMBOLIC alleles=[A*, <NON_REF>] attr={END=640159} GT=[[W22 A* GQ 0 DP 9 PL 0,0 {MIN_DP=9}]]

####Error at 640192

ASM:[VC Unknown @ 9:640192 Q. of type=SNP alleles=[G*, C] attr={} GT=[[W22_Assembly C]]

HCV:[VC Unknown @ 9:640187-640201 Q. of type=SYMBOLIC alleles=[A*, <NON_REF>] attr={END=640201} GT=[[W22 A* GQ 45 DP 7 PL 0,45 {MIN_DP=7}]]


####Error at 640223

ASM:[VC Unknown @ 9:640223 Q. of type=SNP alleles=[T*, G] attr={} GT=[[W22_Assembly G]]

HCV:[VC Unknown @ 9:640223 Q. of type=SYMBOLIC alleles=[T*, <NON_REF>] attr={END=640223} GT=[[W22 T* GQ 99 DP 8 PL 0,225 {MIN_DP=8}]]

####Error at 640224

ASM:[VC Unknown @ 9:640224 Q. of type=SNP alleles=[G*, T] attr={} GT=[[W22_Assembly T]]

HCV:[VC Unknown @ 9:640224 Q. of type=SYMBOLIC alleles=[G*, <NON_REF>] attr={END=640224} GT=[[W22 G* GQ 0 DP 8 PL 0,0 {MIN_DP=8}]]

####Error at 640229

ASM:[VC Unknown @ 9:640229 Q. of type=SNP alleles=[C*, G] attr={} GT=[[W22_Assembly G]]

HCV:[VC Unknown @ 9:640229 Q. of type=SYMBOLIC alleles=[C*, <NON_REF>] attr={END=640229} GT=[[W22 C* GQ 0 DP 7 PL 0,0 {MIN_DP=7}]]



###INDELs



####Error at 322404

ASM:[VC Unknown @ 9:322404-322405 Q. of type=INDEL alleles=[TC*, T] attr={} GT=[[W22_Assembly T]]

HCV:[VC Unknown @ 9:322404-322405 Q. of type=SYMBOLIC alleles=[T*, <NON_REF>] attr={END=322405} GT=[[W22 T* GQ 0 DP 13 PL 0,0 {MIN_DP=12}]]


####Error at 322800

ASM:[VC Unknown @ 9:322800 Q. of type=INDEL alleles=[C*, CACT] attr={} GT=[[W22_Assembly CACT]]

HCV:[VC Unknown @ 9:322799-322801 Q. of type=SYMBOLIC alleles=[A*, <NON_REF>] attr={END=322801} GT=[[W22 A* GQ 99 DP 15 PL 0,514 {MIN_DP=15}]]


####Error at 322404

ASM:[VC Unknown @ 9:322404-322405 Q. of type=INDEL alleles=[TC*, T] attr={} GT=[[W22_Assembly T]]

HCV:[VC Unknown @ 9:322404-322405 Q. of type=SYMBOLIC alleles=[T*, <NON_REF>] attr={END=322405} GT=[[W22 T* GQ 0 DP 13 PL 0,0 {MIN_DP=12}]]

####Error at 322405

ASM:[VC Unknown @ 9:322404-322405 Q. of type=INDEL alleles=[TC*, T] attr={} GT=[[W22_Assembly T]]

HCV:[VC Unknown @ 9:322404-322405 Q. of type=SYMBOLIC alleles=[T*, <NON_REF>] attr={END=322405} GT=[[W22 T* GQ 0 DP 13 PL 0,0 {MIN_DP=12}]]

####Error at 322800

ASM:[VC Unknown @ 9:322800 Q. of type=INDEL alleles=[C*, CACT] attr={} GT=[[W22_Assembly CACT]]

HCV:[VC Unknown @ 9:322799-322801 Q. of type=SYMBOLIC alleles=[A*, <NON_REF>] attr={END=322801} GT=[[W22 A* GQ 99 DP 15 PL 0,514 {MIN_DP=15}]]

####Error at 639798 DP=0, this should be missing also seems repetitive in B73

ASM:[VC Unknown @ 9:639798-639834 Q. of type=INDEL alleles=[CTATATATATATATATATATATATATATATATATATA*, C] attr={} GT=[[W22_Assembly C]]

HCV:[VC Unknown @ 9:639746-639900 Q. of type=SYMBOLIC alleles=[A*, <NON_REF>] attr={END=639900} GT=[[W22 A* GQ 0 DP 0 PL 0,0 {MIN_DP=0}]]

####Error at 639934

ASM:[VC Unknown @ 9:639934 Q. of type=INDEL alleles=[G*, GT] attr={} GT=[[W22_Assembly GT]]

HCV:[VC Unknown @ 9:639934-639935 Q. of type=SYMBOLIC alleles=[G*, <NON_REF>] attr={END=639935} GT=[[W22 G* GQ 0 DP 4 PL 0,0 {MIN_DP=2}]]

####Error at 640181

ASM:[VC Unknown @ 9:640181-640188 Q. of type=INDEL alleles=[AAGGCGAG*, A] attr={} GT=[[W22_Assembly A]]

HCV:[VC Unknown @ 9:640160-640181 Q. of type=SYMBOLIC alleles=[C*, <NON_REF>] attr={END=640181} GT=[[W22 C* GQ 99 DP 9 PL 0,135 {MIN_DP=8}]]


####Error at 640193
ASM:[VC Unknown @ 9:640193-640194 Q. of type=INDEL alleles=[AG*, A] attr={} GT=[[W22_Assembly A]]
HCV:[VC Unknown @ 9:640187-640201 Q. of type=SYMBOLIC alleles=[A*, <NON_REF>] attr={END=640201} GT=[[W22 A* GQ 45 DP 7 PL 0,45 {MIN_DP=7}]]


####Error at 640214

ASM:[VC Unknown @ 9:640214-640217 Q. of type=INDEL alleles=[GCGC*, G] attr={} GT=[[W22_Assembly G]]

HCV:[VC Unknown @ 9:640202-640219 Q. of type=SYMBOLIC alleles=[G*, <NON_REF>] attr={END=640219} GT=[[W22 G* GQ 0 DP 8 PL 0,0 {MIN_DP=7}]]




