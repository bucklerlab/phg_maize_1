#Notebook For HC SNP, ASM Ref Errors

###Running Chromosome 9 of W22 through the Mummer pipeline
* Normalized INDELs
* Resized surrounding Reference Blocks in GVCF for the normalized indels using ResizeRefBlockPlugin currently on PHG-201 branch

Using Ed's counting groovy script.  CompareHCWAssembly.groovy.  Reported Positions are in B73 agpv4 coordinates provided by the GVCF


###Discrepancies

####Error at 322401

ASM:[VC Unknown @ 9:322400-322402 Q. of type=NO_VARIATION alleles=[C*] attr={END=322402} GT=[[W22_Assembly C*]]

HCV:[VC Unknown @ 9:322401 Q510.00 of type=MIXED alleles=[C*, <NON_REF>, T] attr={DP=12, MLEAC=[1, 0], MLEAF=[1.000, 0.000], RAW_MQ=43200.00} GT=[[W22 T GQ 99 DP 12 AD 0,12,0 PL 540,0,540 {SB=0,0,4,8}]]

###INDELS

####Error at 322389

ASM:[VC Unknown @ 9:322373-322390 Q. of type=NO_VARIATION alleles=[A*] attr={END=322390} GT=[[W22_Assembly A*]]

HCV:[VC Unknown @ 9:322389-322399 Q500.97 of type=MIXED alleles=[TCAACACCCCC*, <NON_REF>, T] attr={DP=13, MLEAC=[1, 0], MLEAF=[1.000, 0.000], RAW_MQ=46800.00} GT=[[W22 T GQ 99 DP 12 AD 0,12,0 PL 540,0,540 {SB=0,0,4,8}]]

