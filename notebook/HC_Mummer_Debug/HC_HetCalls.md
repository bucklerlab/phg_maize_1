#Notebook For HC calls which look like hets

###Running Chromosome 9 of W22 through the Mummer pipeline
* Normalized INDELs
* Resized surrounding Reference Blocks in GVCF for the normalized indels using ResizeRefBlockPlugin currently on PHG-201 branch

Using Ed's counting groovy script.  CompareHCWAssembly.groovy.  Reported Positions are in B73 agpv4 coordinates provided by the GVCF

###Discrepancies




####Error at 333886

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:333886 Q833.00 of type=MIXED alleles=[G*, <NON_REF>, A] attr={BaseQRankSum=-2.623, ClippingRankSum=0.000, DP=32, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=0.000, RAW_MQ=114625.00, ReadPosRankSum=-0.027} GT=[[W22 A GQ 99 DP 31 AD 5,26,0 PL 863,0,1159 {SB=1,4,12,14}]]

####Error at 333913

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:333913 Q883.00 of type=MIXED alleles=[T*, <NON_REF>, G] attr={BaseQRankSum=-1.445, ClippingRankSum=0.000, DP=33, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=0.000, RAW_MQ=118225.00, ReadPosRankSum=0.574} GT=[[W22 G GQ 99 DP 31 AD 5,26,0 PL 913,0,1141 {SB=1,4,10,16}]]

####Error at 333980

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:333980 Q105.00 of type=MIXED alleles=[G*, <NON_REF>, A] attr={BaseQRankSum=-1.237, ClippingRankSum=0.000, DP=34, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-1.175, RAW_MQ=120420.00, ReadPosRankSum=-1.983} GT=[[W22 A GQ 99 DP 31 AD 13,18,0 PL 135,0,810 {SB=3,10,10,8}]]

####Error at 333991

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:333991 Q645.00 of type=MIXED alleles=[C*, <NON_REF>, T] attr={BaseQRankSum=0.219, ClippingRankSum=0.000, DP=30, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-0.706, RAW_MQ=106020.00, ReadPosRankSum=-0.292} GT=[[W22 T GQ 99 DP 27 AD 6,21,0 PL 675,0,945 {SB=3,3,8,13}]]

####Error at 333998

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:333998 Q20.05 of type=MIXED alleles=[G*, <NON_REF>, T] attr={BaseQRankSum=0.352, ClippingRankSum=0.000, DP=28, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-1.221, RAW_MQ=98820.00, ReadPosRankSum=-1.288} GT=[[W22 T GQ 50 DP 25 AD 11,14,0 PL 50,0,540 {SB=4,7,7,7}]]

####Error at 334012

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:334012 Q453.00 of type=MIXED alleles=[A*, <NON_REF>, G] attr={BaseQRankSum=0.930, ClippingRankSum=0.000, DP=25, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-0.707, RAW_MQ=88020.00, ReadPosRankSum=-1.454} GT=[[W22 G GQ 99 DP 22 AD 5,17,0 PL 483,0,708 {SB=3,2,7,10}]]

####Error at 334045

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:334045 Q227.00 of type=MIXED alleles=[A*, <NON_REF>, G] attr={BaseQRankSum=1.410, ClippingRankSum=0.000, DP=22, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-0.905, RAW_MQ=77220.00, ReadPosRankSum=0.440} GT=[[W22 G GQ 99 DP 19 AD 6,13,0 PL 257,0,527 {SB=3,3,7,6}]]

####Error at 334090

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:334090 Q105.00 of type=MIXED alleles=[A*, <NON_REF>, G] attr={BaseQRankSum=2.011, ClippingRankSum=-0.000, DP=13, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-1.294, RAW_MQ=44701.00, ReadPosRankSum=0.002} GT=[[W22 G GQ 99 DP 13 AD 5,8,0 PL 135,0,360 {SB=2,3,5,3}]]

####Error at 334091

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:334091 Q105.00 of type=MIXED alleles=[A*, <NON_REF>, G] attr={BaseQRankSum=2.011, ClippingRankSum=-0.000, DP=13, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-1.294, RAW_MQ=44701.00, ReadPosRankSum=0.002} GT=[[W22 G GQ 99 DP 13 AD 5,8,0 PL 135,0,360 {SB=2,3,5,3}]]

####Error at 334099

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:334099 Q105.00 of type=MIXED alleles=[G*, <NON_REF>, A] attr={BaseQRankSum=1.828, ClippingRankSum=-0.000, DP=13, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-1.294, RAW_MQ=44701.00, ReadPosRankSum=-0.425} GT=[[W22 A GQ 99 DP 13 AD 5,8,0 PL 135,0,360 {SB=2,3,5,3}]]

####Error at 335052

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:335052 Q54.00 of type=MIXED alleles=[C*, <NON_REF>, T] attr={BaseQRankSum=1.685, ClippingRankSum=0.000, DP=34, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=0.000, RAW_MQ=122400.00, ReadPosRankSum=-1.412} GT=[[W22 T GQ 84 DP 34 AD 18,16,0 PL 84,0,599 {SB=9,9,9,7}]]

####Error at 335071

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:335071 Q16.11 of type=MIXED alleles=[T*, <NON_REF>, C] attr={BaseQRankSum=1.048, ClippingRankSum=0.000, DP=33, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=0.000, RAW_MQ=118800.00, ReadPosRankSum=-3.076} GT=[[W22 C GQ 46 DP 33 AD 18,15,0 PL 46,0,541 {SB=9,9,7,8}]]

####Error at 335176

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:335176 Q343.00 of type=MIXED alleles=[T*, <NON_REF>, C] attr={BaseQRankSum=0.338, ClippingRankSum=0.000, DP=40, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-0.309, RAW_MQ=139410.00, ReadPosRankSum=1.257} GT=[[W22 C GQ 99 DP 39 AD 16,23,0 PL 373,0,1035 {SB=9,7,8,15}]]

####Error at 335421

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:335421 Q20.05 of type=MIXED alleles=[A*, <NON_REF>, G] attr={BaseQRankSum=0.674, ClippingRankSum=0.000, DP=44, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-2.545, RAW_MQ=156659.00, ReadPosRankSum=2.127} GT=[[W22 G GQ 50 DP 44 AD 24,20,0 PL 50,0,730 {SB=12,12,11,9}]]

####Error at 335479

ASM:[VC Unknown @ 9:332003-335970 Q. of type=NO_VARIATION alleles=[T*] attr={END=335970} GT=[[W22_Assembly T*]]

HCV:[VC Unknown @ 9:335479 Q201.00 of type=MIXED alleles=[A*, <NON_REF>, G] attr={BaseQRankSum=-1.891, ClippingRankSum=0.000, DP=42, MLEAC=[1, 0], MLEAF=[1.000, 0.000], MQRankSum=-2.011, RAW_MQ=149459.00, ReadPosRankSum=-0.255} GT=[[W22 G GQ 99 DP 42 AD 18,24,0 PL 231,0,1056 {SB=6,12,13,11}]]

####Error at 335971

ASM:[VC Unknown @ 9:335971 Q. of type=SNP alleles=[G*, T] attr={} GT=[[W22_Assembly T]]

HCV:[VC Unknown @ 9:335971 Q0.00 of type=MIXED alleles=[G*, <NON_REF>, T] attr={BaseQRankSum=1.245, ClippingRankSum=0.000, DP=48, MLEAC=[0, 0], MLEAF=[0.000, 0.000], MQRankSum=0.000, RAW_MQ=172800.00, ReadPosRankSum=0.251} GT=[[W22 G* GQ 99 DP 44 AD 20,24,0 PL 0,99,892 {SB=12,8,12,12}]]
