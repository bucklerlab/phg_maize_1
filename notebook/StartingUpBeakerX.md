Instructions for setting up BeakerX

Install Docker

Setup a three folders for mounting to the docker:
* jars: TASSEL and PHG jars
* data: data to be analyzed
* code: this repository with the notebooks

docker run -v /Users/edbuckler/temp/phgjars/:/home/beakerx/jars/ -v /Users/edbuckler/temp/mlw22reads/:/home/beakerx/data/ -v /Users/edbuckler/Code/phg_maize_1/:/home/beakerx/code/ -p 8888:8888 -it beakerx/beakerx
